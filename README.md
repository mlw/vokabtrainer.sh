# !! Notice !!

This has been replaced by [krabbe](https://codeberg.org/mlw/krabbe)

# vokabtrainer

A POSIX shell Vocabulary training script that reads colon-seperated lines from a file.

## Install

Just run

```
$ make userinstall
```

(to install to ~/.local/bin)

```
# make install
```

(to install to /usr/local/bin)

## Usage

```
$ vokabtrainer vocabfile [vocabfiles...]
```

An example vocabfile 'test.txt' is included.

The file format should be self-explanatory.

## Where does the name come from

From German: 'vokabeln' means vocabulary

vokabeln + trainer = vokabtrainer

## BUGS

* can only read up to 4 other translations per line
* because tr doesn't convert all letters to lowercase(like accented letters), you should try to write everything in lowercase by default.
